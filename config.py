from gi.repository import Gio, GObject, PeasGtk, Gtk

DCONF_DIR = 'org.gnome.rhythmbox.plugins.books'


class ConfigMenu(GObject.Object, PeasGtk.Configurable):
    __gtype_name__ = 'booksConfigDialog'
    object = GObject.property(type=GObject.Object)

    def __init__(self):
        GObject.Object.__init__(self)
        self.settings = Gio.Settings(DCONF_DIR)

    def do_create_configure_widget(self):

        page = Gtk.VBox()
        hbox = Gtk.HBox()

        switch = Gtk.Switch()
        switch.set_active(self.settings["remove-entries"])
        switch.connect("notify::active", self.switch_toggled, "remove-entries")

        label = Gtk.Label("<b>" + _("Remove entries ") + "</b>")
        label.set_use_markup(True)

        descr = Gtk.Label(
            "<i>" + _("Whether to automatically remove entries which have no file on disk") + "</i>")
        descr.set_alignment(0, 0)
        descr.set_margin_left(25)
        descr.set_margin_right(25)
        descr.set_line_wrap(True)
        descr.set_use_markup(True)

        hbox.pack_start(label, False, False, 10)
        hbox.pack_start(switch, False, False, 10)

        vbox = Gtk.VBox()
        vbox.pack_start(hbox, False, False, 0)
        vbox.pack_start(descr, False, False, 0)

        label = Gtk.Label("<b>" + _("Remove entries") + "</b>")
        label.set_use_markup(True)

        hbox = Gtk.HBox()

        label = Gtk.Label("<b>" + ("Books folder") + "</b>")
        label.set_use_markup(True)

        file_chooser = Gtk.FileChooserButton()
        file_chooser.set_action(Gtk.FileChooserAction.SELECT_FOLDER)
        file_chooser.set_current_folder(self.settings["books-folder"])
        file_chooser.connect("current-folder-changed", self.folder_set)

        hbox.pack_start(label, False, False, 10)
        hbox.pack_start(file_chooser, True, True, 10)

        vbox.pack_start(hbox, False, False, 15)

        page.pack_start(vbox, True, True, 15)

        nb = Gtk.Notebook()
        nb.set_show_tabs(False)
        nb.append_page(page, Gtk.Label(("General")))

        nb.show_all()
        nb.set_size_request(450, -1)

        return nb

    def switch_toggled(self, switch, active, key):
        self.settings[key] = switch.get_active()

    def folder_set(self, file_chooser):
        new_folder = file_chooser.get_current_folder()
        if self.settings["books-folder"] != new_folder:
            self.settings["books-folder"] = new_folder
