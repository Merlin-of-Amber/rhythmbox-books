from gi.repository import RB, GObject, Gtk, Gdk, GLib
from book import Book
import datetime
import os
import re
import eyed3
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
from mutagen.flac import FLAC
import utils
from pprint import pprint
from browser import GtkBrowser


class BooksSource(RB.BrowserSource):
    def __init__(self):
        RB.BrowserSource.__init__(self)

        self.activated = False
        self.entryView = self.get_entry_view()
        # self.bind_settings(None, None, GtkBrowser, True)

    def do_selected(self):

        if not self.activated:
            self.activated = True

            self.entry_view = self.get_entry_view()
            self.entry_view.get_column(
                RB.EntryViewColumn.ALBUM).set_title("Book")
            self.entry_view.get_column(
                RB.EntryViewColumn.ARTIST).set_title("Author")

            self.entry_view.set_column_editable(RB.EntryViewColumn.ALBUM, True)
            # self.entry_view.set_column_editable(
            #     RB.EntryViewColumn.ARTIST, True)
            # self.entry_view.set_column_editable(RB.EntryViewColumn.GENRE, True)
            # self.entry_view.set_column_editable(RB.EntryViewColumn.TITLE, True)
            # self.entry_view.set_column_editable(
            #     RB.EntryViewColumn.TRACK_NUMBER, True)

            self.entry_view.set_sorting_order("Track", Gtk.SortType.ASCENDING)

            self.props.show_browser = True

            self.property_view = self.get_property_views()

            artist = self.property_view[0]
            album = self.property_view[1]

            artist.set_min_content_height(365)

            self.run_idle()

    def do_impl_delete_thyself(self):
        if self.activated:
            self.shell = False

    def run_idle(self):

        Gdk.threads_init()

        Gdk.threads_add_idle(GLib.PRIORITY_DEFAULT_IDLE,
                             self.check_deleted_files)
        Gdk.threads_add_idle(GLib.PRIORITY_DEFAULT_IDLE,
                             self.scan_books_folder)
        Gdk.threads_add_idle(GLib.PRIORITY_DEFAULT_IDLE, self.monitor_tags)

    def add_book(self, book, entry):

        if book.track_number:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.TRACK_NUMBER, book.track_number)

        if book.title:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.TITLE, book.title)

        if book.genre:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.GENRE, book.genre)

        if book.artist:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.ARTIST, book.artist)

        if book.album:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.ALBUM, book.album)

        if book.year:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.DATE, datetime.date(book.year, 1, 1).toordinal())

        if book.comment:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.COMMENT, book.comment)

        if book.duration:
            self.props.plugin.db.entry_set(
                entry, RB.RhythmDBPropType.DURATION, book.duration)

        self.props.plugin.db.commit()
        self.props.query_model.add_entry(entry, -1)

    def scan_books_folder(self):

        # start = datetime.datetime.now()

        uri = self.props.plugin.config.settings["books-folder"]

        for root, dirs, files in os.walk(uri):
            for file in files:
                if file.endswith(".mp3") or file.endswith(".flac"):

                    uri = "file://" + root + "/" + file

                    if not self.props.plugin.db.entry_lookup_by_location(uri):

                        if file.endswith(".mp3"):
                            audio = EasyID3(root + "/" + file)
                        else:
                            audio = FLAC(root + "/" + file)

                        try:
                            tracknumber = int(
                                re.search('^[0-9]+', audio["tracknumber"][0])[0])
                        except KeyError:
                            tracknumber = None

                        try:
                            title = audio["title"][0]
                        except KeyError:
                            title = None

                        try:
                            genre = audio["genre"][0]
                        except KeyError:
                            genre = None

                        try:
                            artist = audio["artist"][0]
                        except KeyError:
                            artist = None

                        try:
                            album = audio["album"][0]
                        except KeyError:
                            album = None

                        try:
                            year = int(audio["date"][0])
                        except KeyError:
                            year = None

                        if file.endswith(".mp3"):
                            audio = MP3(root + "/" + file)

                        length = int(audio.info.length)
                        bitrate = int(audio.info.bitrate/1000)

                        book = Book(
                            tracknumber,
                            title,
                            genre,
                            artist,
                            album,
                            year,
                            length,
                            None,  # test
                        )

                        entry = RB.RhythmDBEntry(
                            self.props.plugin.db, self.props.plugin.entry_type, uri)
                        self.add_book(book, entry)

        # print(datetime.datetime.now() -start)

        Gdk.threads_add_timeout(
            GLib.PRIORITY_DEFAULT_IDLE, 1000, self.scan_books_folder)

    def check_deleted_files(self):

        for book in self.props.query_model:

            uri = book[0].get_string(RB.RhythmDBPropType.LOCATION)
            location = re.sub("^[/]+", "/", uri.__str__()[6:])

            if not (os.path.isfile(location)) and self.props.plugin.config.settings["remove-entries"]:
                self.props.plugin.db.entry_delete(book[0])
                self.props.plugin.db.commit()
                # self.props.query_model.remove_entry(book[0])

        Gdk.threads_add_timeout(
            GLib.PRIORITY_DEFAULT_IDLE, 1000, self.check_deleted_files)

    def monitor_tags(self):

        # start = datetime.datetime.now()

        for book in self.props.query_model:

            uri = book[0].get_string(RB.RhythmDBPropType.LOCATION)
            location = re.sub("^[/]+", "/", uri.__str__()[6:])

            # audio = EasyID3(location)

            if location.endswith(".mp3"):
                audio = EasyID3(location)
            else:
                audio = FLAC(location)

            tags = {
                "title": RB.RhythmDBPropType.TITLE,
                "genre": RB.RhythmDBPropType.GENRE,
                "artist": RB.RhythmDBPropType.ARTIST,
                "album": RB.RhythmDBPropType.ALBUM,
            }

            for k, v in tags.items():

                try:
                    original = audio[k][0]

                    if original is not book[0].get_string(v):
                        utils.entry_set(self.props.plugin.db,
                                        book[0], v, original)
                except Exception as e:
                    print("EXCEPTION - " + e.__str__())

            # year
            try:
                original = datetime.date(
                    int(audio["date"][0]), 1, 1).toordinal()

                if original is not book[0].get_ulong(RB.RhythmDBPropType.DATE):
                    utils.entry_set(self.props.plugin.db,
                                    book[0], RB.RhythmDBPropType.DATE, original)
            except Exception as e:
                print("EXCEPTION - " + e.__str__())

            # tracknumber
            try:
                original = int(
                    re.search('^[0-9]+', audio["tracknumber"][0])[0])

                if original is not book[0].get_ulong(RB.RhythmDBPropType.TRACK_NUMBER):
                    utils.entry_set(
                        self.props.plugin.db, book[0], RB.RhythmDBPropType.TRACK_NUMBER, original)
            except Exception as e:
                print("EXCEPTION - " + e.__str__())

        # print(datetime.datetime.now() - start)

        Gdk.threads_add_timeout(
            GLib.PRIORITY_DEFAULT_IDLE, 5000, self.monitor_tags)
