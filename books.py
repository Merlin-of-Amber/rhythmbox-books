from gi.repository import Gdk, GLib, RB, GdkPixbuf, GObject, Peas
import rb
import re
import os
import sys
from config import ConfigMenu
from source import BooksSource


class Books (GObject.Object, Peas.Activatable):
    object = GObject.property(type=GObject.Object)

    def __init__(self):
        super(Books, self).__init__()

    def do_activate(self):

        self.shell = self.object
        self.config = ConfigMenu()

        self.entry_type = BookEntryType()

        self.db = self.shell.props.db
        self.db.register_entry_type(self.entry_type)

        icon_name = "/icons/icon.svg"
        icon_path = sys.path[0] + icon_name

        icon_file = rb.find_plugin_file(self, icon_path)
        icon = GdkPixbuf.Pixbuf.new_from_file(icon_file)

        self.books_source = GObject.new(BooksSource,
                                        shell=self.shell,
                                        icon=icon,
                                        name=("Books"),
                                        plugin=self,
                                        entry_type=self.entry_type,
                                        )

        group = RB.DisplayPageGroup.get_by_id("library")
        self.shell.append_display_page(self.books_source, group)
        self.shell.register_entry_type_for_source(
            self.books_source, self.entry_type)

    def do_deactivate(self):

        self.books_source.delete_thyself()
        self.books_source = None


class BookEntryType(RB.RhythmDBEntryType):
    def __init__(self):
        RB.RhythmDBEntryType.__init__(self, name='book', save_to_disk=True)


GObject.type_register(BooksSource)
