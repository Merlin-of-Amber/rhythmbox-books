from gi.repository import RB, GObject, Gtk, Gdk, GLib

class GtkBrowser(Gtk.Widget):

     def __init__(self):
        Gtk.Widget.__init__(self)

        self.activated = False
        self.set_size_request(150, 150)
        self.size_request()