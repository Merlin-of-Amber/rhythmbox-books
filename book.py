class Book:

    def __init__(self, track_number, title, genre, artist, album, year, duration, comment):

        self.track_number = track_number
        self.title = title
        self.genre = genre
        self.artist = artist
        self.album = album
        self.year = year
        self.duration = duration
        self.comment = comment